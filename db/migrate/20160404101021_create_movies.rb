class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :name
      t.string :language
      t.string :mpr
      t.string :review

      t.timestamps null: false
    end
  end
end
