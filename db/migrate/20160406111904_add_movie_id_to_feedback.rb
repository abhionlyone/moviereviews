class AddMovieIdToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :movie_id, :integer
  end
end
