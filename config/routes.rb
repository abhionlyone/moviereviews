Rails.application.routes.draw do
  devise_for :users
  root "movies#index"
  get 'pages/about' => "pages#About"
  get 'pages/contact' =>"pages#Contact"
  resources :movies do
  	resources :feedbacks, except: [:show,:index]
  end
 end
