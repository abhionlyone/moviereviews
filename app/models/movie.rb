class Movie < ActiveRecord::Base
	  before_save :update_slug
	  mount_uploader :poster, PosterUploader
	  has_many :feedbacks


end
