class FeedbacksController < ApplicationController
  before_action :set_feedback, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!,only: [:edit,:update,:destroy,:new]
  before_action :set_movie

  # GET /feedbacks/new
  def new
    #@feedback = Feedback.new
    @feedback = current_user.feedbacks.build

  end

  # GET /feedbacks/1/edit
  def edit
    if current_user != @feedback.user
     redirect_to @feedback, notice: "You aren't authorized to do this"    
    else
    end
  end

  # POST /feedbacks
  # POST /feedbacks.json
  def create
    #@feedback = Feedback.new(feedback_params)
    #@feedback.user_id = current_user.id
    @feedback = current_user.feedbacks.build(feedback_params)
    @feedback.movie_id = @movie.id

    respond_to do |format|
      if @feedback.save
        format.html { redirect_to @movie, notice: 'Feedback was successfully created.' }
        format.json { render :show, status: :created, location: @feedback }
      else
        format.html { render :new }
        format.json { render json: @feedback.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /feedbacks/1
  # PATCH/PUT /feedbacks/1.json
  def update
    respond_to do |format|
        if @feedback.update(feedback_params)
          format.html { redirect_to root_path, notice: 'Feedback was successfully updated.' }
          format.json { render :show, status: :ok, location: @feedback }
        else
          format.html { render :edit }
          format.json { render json: @feedback.errors, status: :unprocessable_entity }
        end
    end
  end

  # DELETE /feedbacks/1
  # DELETE /feedbacks/1.json
  def destroy
    if @feedback.user==current_user
      @feedback.destroy
      respond_to do |format|
        format.html { redirect_to feedbacks_url, notice: 'Feedback was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to feedbacks_url, notice: "You aren't authorized to do this"}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feedback
      @feedback = Feedback.find(params[:id])
    end

    def set_movie
      @movie = Movie.find(params[:movie_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def feedback_params
      params.require(:feedback).permit(:rating, :comment, :name)
    end
end
