json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :language, :mpr, :review
  json.url movie_url(movie, format: :json)
end
